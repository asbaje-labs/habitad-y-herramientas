# Habitad y Herramientas

> Estudios sobre la organización física de mi casa, oficina y otros entornos donde desarrollo mis tareas, en aras de incrementar mi productividad personal.

> **Interior Design** is the art and science of enhancing the interior of a space, encompassing the aesthetic, functional, and spatial aspects to create a well-designed and harmonious environment that meets the needs and preferences of its occupants.

> **Interiors Design Models**:  Interior design models encompass visual representations and conceptual frameworks used by designers to plan, organize, and communicate spatial arrangements, aesthetics, and functional elements within interior spaces.

## Pared

- [Combinación tablero de clavijas, blanco, 14 ¼x22 “](https://www.ikea.com.do/es/pd/skadis-combinacion-tablero-de-clavijas-blanco-spr-09217123)
    - [Canasta de almacenaje, juego de 3, blanco](https://www.ikea.com.do/es/pd/skadis-canasta-de-almacenaje-juego-de-3-blanco-art-50517760)
- [Estante de pared, marrón-negro/álamo teñido blanco, 47 1/4x11 3/4 “](https://www.ikea.com.do/es/pd/bergshult-sandshult-estante-de-pared-marron-negro-spr-49326016)
    - Poner Ananico
    - Poner Lampara
    - Ordenar los cable scon un “protector de cables“ / Limpieza Visual
    - [Foco de pared/con abrazadera, gris oscuro](https://www.ikea.com.do/es/pd/hektar-foco-de-pared-con-abrazadera-gris-oscuro-art-50216540)

## Otros

- [Tijeras, juego de 2](https://www.ikea.com.do/es/pd/markbart-tijeras-juego-de-2-art-40328557)
- [Caja de Herramientas](https://www.ikea.com.do/es/pd/rakkestad-ropero-3-puertas46-1-8-x21-5-8-x-69-1-4-negro-marron-art-00453765)
- Cortinas
    - [Cortina opaca, par, gris, 57x98 “](https://www.ikea.com.do/es/pd/hilleborg-cortinas-opacas-1-par-gris-art-40425030)
    - [Juego de barra de cortina doble, negro, 47-83 “](https://www.ikea.com.do/es/pd/bekrafta-juego-de-barra-de-cortina-doble-negro-art-70489695)
- [Etiquetas](https://www.amazon.com/-/es/D11/dp/B0B4W92J8V/ref=dp_prsubs_sccl_1/145-0147924-0124729?pd_rd_w=uDpsi&content-id=amzn1.sym.53e0c629-1936-47cb-93a2-c361b12e7d3c&pf_rd_p=53e0c629-1936-47cb-93a2-c361b12e7d3c&pf_rd_r=3M6XC1XZWQE7PMZJJQYT&pd_rd_wg=7TOkB&pd_rd_r=64f1289f-7fd5-4495-836f-88dda4b07e68&pd_rd_i=B088JTD1PZ&th=1)
- [Cargador Portatil](https://www.amazon.com/-/es/Cargador-port%C3%A1til-velocidad-linterna-compatible/dp/B07CZDXDG8/?th=1)
- BOLMEN  Taburete negro
- BEKVÄM  Taburete con peldaños haya
- 

## References

- [Habitat](https://en.wikipedia.org/wiki/Habitat)
- [Interior Design](https://en.wikipedia.org/wiki/Interior_design)
- [20 Interior Design Styles You Need to Know: A Comprehensive Guide](https://coolaiid.com/blog/20-interior-design-styles-you-need-to-know-a-comprehensive-guide.html)
- [Self-Taught Interior Design: Where to look?](https://www.reddit.com/r/InteriorDesign/comments/31k4r0/comment/cq35xl8/)
- [Ultimate Setup With Ikea Desk for Gaming](https://www.minimaldesksetups.com/the-ultimate-setup-with-ikea-desk-for-gaming/)
- Organizar el Refrigerador
https://www.youtube.com/shorts/CSEWo0-68QE