# Dormitorio

## Cama

![NYHAMN  Sofá cama 3 plazas con colchón de espuma Skartofta negro/gris claro](https://static.ikea.com.do/assets/images/553/1155312_PE886552_S5.webp)

## Ropero

![RAKKESTAD  Ropero 3 puertas, negro-marrón, 46 1/8x69 1/4 “](https://static.ikea.com.do/assets/images/239/0823988_PE776019_S5.webp)


    - [Gancho para ropa, negro](https://telepedidos.ikea.com.do/es/pd/bumerang-gancho-para-ropa-negro-art-20238534)
- [Sofá cama 3 plazas, con colchón de espuma/Knisa gris/beige](https://www.ikea.com.do/es/pd/nyhamn-sofa-cama-3-plazas-gris-beige-spr-39306373)
    - [Funda de edredón y funda(s) de almohada, Full Queen, 118 hilos, gris/raya](https://www.ikea.com.do/es/pd/bergpalm-funda-de-edredon-funda-s-de-almohada-118-hilos-full-queen-gris-art-80423265)
    - [Mesa de centro, negro-marrón, 35 3/8x21 5/8 “](https://www.ikea.com.do/es/pd/lack-mesa-de-centro-negro-marron-art-40104294)
    - [Rodillo quitapelusas, gris](https://www.ikea.com.do/es/pd/bastis-rodillo-quitapelusas-gris-art-90425626)
    - [Protector colchón impermeable, Full](https://www.ikea.com.do/es/pd/grusnarv-protector-colchon-impermeable-art-30522140)
    - [Mesa de almacenaje, negro, 17 3/8 “](https://www.ikea.com.do/es/pd/kvistbro-mesa-de-almacenaje-negro-art-90480401)
        - Poner la Ropa Sucia / Encima Libros o Otras Cosas
- …

 KVISTBRO  Mesa de almacenaje negro, 17 3/8"

