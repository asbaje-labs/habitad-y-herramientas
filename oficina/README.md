# Oficina

- [Escritorio sentado/pie, 63x31 1/2 ", chapa fresno teñida en negro/negro, regulable](https://www.ikea.com.do/es/pd/bekant-escritorio-sentado-pie-63-x31-1-2-patas-regulables-con-motor-negro-spr-59281814)
- [Protector de Escritorio](https://www.ikea.com.do/es/pd/rissla-protector-de-escritorio-negro-art-40246156)
- Soporte
    - [Soporte para móvil/tableta, bambú](https://www.ikea.com.do/es/pd/bergenes-soporte-para-movil-tableta-art-10457999)
    - [VIVALLA / Soporte para tableta, bambú](https://www.ikea.com.do/es/pd/vivalla-soporte-para-tableta-art-60515789)
    - [Soporte para monitor de altura fija, chapa de bambú](https://www.ikea.com.do/es/pd/sigfinn-soporte-para-monitor-de-altura-fija-art-20362929)
    - [Base monitor con cajón, blanco](https://www.ikea.com.do/es/pd/elloven-base-monitor-con-cajon-18-1-2-x10-1-4-x4-blanco-art-50474770)
    - [HAVREHOJ Soporte para tableta](https://www.ikea.com.do/es/pd/havrehoj-soporte-para-tableta-art-40534576)
    - [LÅNESPELARE Soporte para auriculares](https://www.ikea.com.do/es/pd/lanespelare-soporte-para-auriculares-art-80571524)
    - …
- Cables
    - Clabes: Protector Wire / Recogecable
    - [Organizador de cables, juego de 3, negro](https://www.ikea.com.do/es/pd/kalkstuff-organizador-de-cables-juego-de-3-blanco-art-30573021)
        - Usar Para Poner Los Mas Vertical Posible Cables de Estandeteria Pared.
    - [Caja para cables+tapa](https://www.ikea.com.do/es/pd/satting-caja-para-cables-tapa-art-90534625)
    - …
- Otros
    - [LÅNESPELARE Taza gaming con tapa y paja, acero inoxidable](https://www.ikea.com.do/es/pd/lanespelare-taza-tapa-y-paja-negro-art-40507846)
    - [LÅNESPELARE Soporte para taza, negro](https://www.ikea.com.do/es/pd/lanespelare-soporte-para-taza-negro-art-90507844)
    - …
- Cargadores y Cables
    - USB Tipo-C
    - [USB-A a USB-micro, gris oscuro, 4 ' 11 “](https://www.ikea.com.do/es/pd/lillhult-usb-a-a-usb-micro-gris-oscuro-art-80527593)
    - …

- [KDD Soporte para tazas de escritorio 4 en 1 – Abrazadera en el colgador de auriculares debajo del escritorio – Soporte giratorio para auriculares desmontable Soporte para vasos para bolígrafos](https://www.amazon.com/dp/B0C77YK94D?th=1)

- 